const express = require("express");
const http = require('http');
const cors = require('cors');
const os = require('os');
const net = require('net');
const sql = require('mssql');
const fs = require('fs');
const url = require('url');
const path = require('path');
const bodyParser = require("body-parser");
const uds = require('underscore')
const fetch = require('node-fetch');
var red_app = express();
var morgan = require('morgan')
const util = require('util');
var moment = require('moment');
var listenPort = 8080
const { PromiseSocket } = require("promise-socket")
var mongoose = require('mongoose')

// DB Connect SQL Global Param
var config_arr = []
var pool_arr = []
var pool_connect = []

var send_socket_arr = []

//CheckConfig
const configDir = 'C://pwa_sync_conf';
if (!fs.existsSync(configDir)) {
    fs.mkdirSync(configDir);
}

var mongo_ip = "127.0.0.1"
var mongo_port = 27017
var mongo_db = "pwa"
var mongo_user = "admin"
var mongo_pass = "admin"


const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);


async function check_file(f_name) {

    if (!fs.existsSync(configDir + '/' + f_name)) {
        //await writeFile(configDir + '/' + f_name,JSON.stringify(default_json))
        var ret = JSON.parse(
            await readFile('./conf_seed/' + f_name)
        );

        await writeFile(configDir + '/' + f_name, JSON.stringify(ret))
        console.log(f_name)
        return ret

    } else {
        var ret = JSON.parse(
            await readFile(configDir + '/' + f_name)
        );
        console.log(f_name)
        return ret
    }
}


// Init dbcon.json
var dbcon = {}
check_file('dbcon.json').then(data => {
    //console.log("\r\n//// DB Con ////")
    //console.log(data)
    dbcon = data;
    init_db_connection()
    init_mongodb_connection()
});

var setting_arr = {}
check_file('setting.json').then(data => {
    console.log("\r\n//// Setting ////")
    console.log(data)
    setting_arr = data;
});

//InitConfig
var info_config = {}
check_file('info.json').then(data => {
    //console.log("\r\n//// Info json ////")
    //console.log(data)
    info_config = data;
});

//InitConfigArray (Multi Connection)
var info_arr = []
check_file('info_arr.json').then(data => {
    //console.log("\r\n//// Info json ////")
    //console.log(data)
    info_arr = data;
});

//init User
var user_arr = []
const userFile = './user.json';
if (!fs.existsSync(userFile)) {

    user_arr = [
        {
            "index": 0,
            "name": "system Admin",
            "username": "admin",
            "password": "admin",
            "type": "admin"
        },
        {
            "index": 1,
            "name": "บัณฑิต คุ้มสวัสดิ์",
            "username": "bandid12",
            "password": "1234",
            "type": "user"
        }
    ]


    fs.writeFileSync("./user.json", JSON.stringify(user_arr));
} else {

    fs.readFile("./user.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        user_arr = JSON.parse(jsonString);
    })
}

//init Template
var template = []
check_file('template.json').then(data => {
    template = data;
});

//InitTemplate3
const templateFile = configDir + '/template_v3.json';
var template3 = []
check_file('template_v3.json').then(data => {
    template3 = data;
});

//InitProvince
var province_data = []
//const province_file = configDir + '/province.json';
check_file('province.json').then(data => {
    province_data = data;
});

// Init scada_tag  scada_tag.json
var scada_tag = {}
check_file('scada_tag.json').then(data => {
    scada_tag = data;
});


//InitQuery  query.json
var query_config = [];
check_file('query.json').then(data => {
    //console.log("\r\n//// Query json ////")
    //console.log(data)
    query_config = data;
});


//InitMapping  mapping.json
var map_config = [];
check_file('mapping.json').then(data => {
    //console.log("\r\n//// Mapping json ////")
    //console.log(data)
    map_config = data;
});


//InitEmail  email.json
var email_json = {}
check_file('email.json').then(data => {

    email_json = data;
});

//Init  default.json
var default_json = {}
check_file('default.json').then(data => {

    default_json = data;
});





//===========================================
//             API For Web      
//===========================================

red_app.use(bodyParser.json({ limit: '200mb' }));
// Add a simple route for static content served from 'public'
red_app.use(bodyParser.urlencoded({ parameterLimit: 100000, limit: '100mb', extended: true }));
red_app.use(cors());
//red_app.options('*', cors());
red_app.use(express.static(__dirname + "/public"));

var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

// setup the logger
red_app.use(morgan('combined', { stream: accessLogStream }))


red_app.post('/uploadmany',(req, res) => {
    //console.log(req.body)
    if(req.body === undefined){
        return res.status(500);
    }


    if(req.body.length > 0){
        var tmp_body = req.body
        for(let i=0; i < tmp_body.length  ; i++){
            //console.log(tmp_body[i])

            //Check Index MapConfig
            var f_index =  uds.findIndex(map_config, function (temp) { return temp.name == tmp_body[i].mappingName; });
            console.log(f_index)

            //Clear Old Mapping
            map_config[f_index].data = []
            
            //Clear CSV Head
            tmp_body[i].data.shift()
            
            // Add new Map
            map_config[f_index].data = tmp_body[i].data            
            
            // Rebuild Topic
        //    for(let j=0){

        //    }

            //tmp_body[i].mappingName
        }

        return res.json({result:"ok"})

    }else{
        return res.status(500);
    }

});


// Download All CSV file (Make CSV by Vuejs)
red_app.post('/downloadmany',(req, res) => {
    var ret_csv = []
    for(let j=0; j < map_config.length ; j++){
        var tmp_map =map_config[j] 
        // console.log()
        
        for(let k=0;k < tmp_map.data.length ; k++){
            delete tmp_map.data[k]['tag']
            delete tmp_map.data[k]['c']
            delete tmp_map.data[k]['dup']
            delete tmp_map.data[k]['collect']
            delete tmp_map.data[k]['topic']
        }

        //Header CSV
        var first_ele = {
            "branch_code": "Branch Code",
            "station_code": "Station Code",
            "station_type": "Station Type",
            "station_name": "Station Name",
            "device_code": "Device Code",
            "name": "Name",
            "code": "Code",
            "map": "MAP"
        } 
        
        const send_csv = [first_ele].concat(tmp_map.data)
        var tmp = {
            data:send_csv,
            mappingName:tmp_map.name
        }
        ret_csv.push(tmp)  
    }


    return res.json(ret_csv)
});


red_app.get('/email', (req, res) => {
    return res.json(email_json);
});

red_app.post('/email', (req, res) => {
    try {
        email_json = req.body
        fs.writeFile(configDir + "/email.json", JSON.stringify(email_json), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "email_json Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "email_json Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});


red_app.get('/default_conf', (req, res) => {
    return res.json(default_json);
});

red_app.post('/default_conf', (req, res) => {
    try {
        default_json = req.body
        console.log(req.body)

        fs.writeFile(configDir + "/default.json", JSON.stringify(default_json), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")
                return res.json({ ret: "Station Default Saved!" });
            }).on('error', (e) => {
                return res.json({ ret: "Station Default Save Fail!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});


red_app.get('/g_info', (req, res) => {
    return res.json(info_arr[0])
});

red_app.post('/g_info', (req, res) => {
    try {
        info_arr[0] = req.body
        fs.writeFile(configDir + "/info.json", JSON.stringify(info_config), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "info_config Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "info_config Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});

// G_info Multiple 

red_app.get('/list_info', (req, res) => {
    return res.json(info_arr)
});

red_app.get('/g_info/:index', (req, res) => {
    console.log(req.params.index)
    if (info_arr[req.params.index] !== undefined) {
        return res.json(info_arr[req.params.index])
    } else {
        return res.status(500).send('Error No index!!!')
    }

});

red_app.post('/g_info/:index', (req, res) => {
    var index = req.params.index
    // info_arr = req.body
    if (info_arr[index] !== undefined) {
        info_arr[index] = req.body
    } else {
        info_arr.push(req.body)
    }

    try {
        //query_config = req.body
        fs.writeFile(configDir + "/info_arr.json", JSON.stringify(info_arr), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "info Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "info Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }

});


red_app.post('/add_info', (req, res) => {
    info_arr.push(req.body)
    try {
        //query_config = req.body
        fs.writeFile(configDir + "/info_arr.json", JSON.stringify(info_arr), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "info Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "info Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }

});


red_app.delete('/g_info/:index', (req, res) => {
    var index = req.params.index

    if (info_arr[index] !== undefined && (index !== undefined)) {

        info_arr.splice(index, 1);

        try {
            //query_config = req.body
            fs.writeFile(configDir + "/info_arr.json", JSON.stringify(info_arr), function (err) {
                if (err) {
                    console.log(err)
                    return res.status(500).json(error);
                };


                http.get('http://localhost:1880/fileupdate', (res2) => {
                    console.log("update nodered ok")
                }).on('error', (e) => {
                    console.error(`Got error: ${e.message}`);
                });
                return res.json({ ret: index + ": info Delete!" });
            });
        } catch (error) {
            console.log(error)
            return res.status(500).json(error);
        }
    } else {
        return res.status(500).send("Error No index!!!");
    }



});


red_app.get('/province', (req, res) => {
    return res.json(province_data)
});

// API  g_query
red_app.get('/g_query', (req, res) => {
    return res.json(query_config)
});

red_app.post('/g_query', (req, res) => {

    try {
        query_config = req.body
        fs.writeFile(configDir + "/query.json", JSON.stringify(query_config), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "query Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "query Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});

// API  setting
red_app.get('/setting', (req, res) => {
    return res.json(setting_arr)
});


red_app.post('/setting', (req, res) => {
    //console.log(req.body)
    try {
        setting_arr = req.body
        fs.writeFile(configDir + "/setting.json", JSON.stringify(map_config), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "setting Saved!" });

            }).on('error', (e) => {
                return res.json({ ret: "setting Saved!" });

            });

        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});


// API  g_mapping
red_app.get('/g_mapping', (req, res) => {
    return res.json(map_config)
});

red_app.post('/g_mapping', (req, res) => {
    console.log("\r\n !!! Mapping Update !!!");
    try {
        map_config = req.body
        console.log(map_config);
        fs.writeFile(configDir + "/mapping.json", JSON.stringify(map_config), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "mapping Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "mapping Saved!" });
                console.error(`Got error: ${e.message}`);
            });

        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }

});

red_app.get('/scada_tag', (req, res) => {
    return res.json(scada_tag)
});

red_app.get('/dbcon', (req, res) => {
    // console.log(req)
    try {
        return res.json(dbcon);
    } catch (error) {
        return res.json(dbcon);
    }
});

red_app.post('/dbcon', (req, res) => {
    console.log(req.body)
    try {
        dbcon = req.body
        console.log(req.body)
        fs.writeFile(configDir + "/dbcon.json", JSON.stringify(req.body), function (err) {
            if (err) {
                console.log(err)
            };
            //dbcon = req.body
            init_db_connection();
            init_mongodb_connection();
            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "dbcon Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "dbcon Saved!" });
                console.error(`Got error: ${e.message}`);
            });

        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }

});

red_app.get('/dbcon_list', (req, res) => {
    // console.log(req)
    try {
        return res.json(config_arr);

    } catch (error) {
        return res.json(config_arr);
    }
});

red_app.post('/reload_db', (req, res) => {
    init_mongodb_connection()
    init_db_connection();
    return res.json({});
});









async function gen_query() {
    var list_table_cmd = "select schema_name(t.schema_id) as schema_name,t.name as table_name from sys.tables t;"
    var list_colume_type = "select COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='SELECT_TABLE'"

    var query_list = []
    for (const con_info of config_arr) {
        let result_data = await messageHandler(con_info.index, list_table_cmd)
        console.log("DฺB :" + con_info.database)
        console.log(result_data)
        try {
            console
            if (result_data.payload.length > 0) {
                await result_data.payload.forEach((item) => {
                    query_list.push({
                        sql: "SELECT TOP(1000) * from dbo." + item.table_name + ";",
                        tbl: item.table_name,
                        index: con_info.index,
                        con_name: config_arr[con_info.index].con_name
                    })
                    //console.log(list_colume_type.replace("SELECT_TABLE",item.table_name));
                });
            }
        } catch (error) {
        }
    }


    return query_list
}

red_app.post('/genquery', (req, res) => {
    console.log("\r\n\r\n/// Gen Query ///")
    // console.log(info_arr)


    gen_query().then(search_query => {
        console.log(search_query)

        loop_gen(search_query).then(function (data) {

            //     return res.json(data)
        });
        return res.json(search_query)
    });




});


red_app.post('/testconn', async (req, res) => {
    console.log("\r\nTEST Con A")
    console.log(req.body)

    try {
        // make sure that any items are correctly URL encoded in the connection string
        // var url = 'mssql://' + req.body.user + ':' + req.body.pass + '@' + req.body.ip + '/' + req.body.db_name
        // var test_arr = []

        var check_db = {
            index: 1,
            con_name: "TEST",
            user: req.body.user,
            password: req.body.pass,
            server: req.body.ip,
            database: req.body.db_name,
            connectionTimeout: 800,
            pool: {
                max: 1,
                min: 0,
                idleTimeoutMillis: 800
            }
        }
        // test_arr.push(check_number)
        var s_pool = new sql.ConnectionPool(check_db);
        
        var tmp_connect = s_pool.connect(err => {
            if (err != null) {
                console.log("\r\nDB Connect Error")
                console.log(err.code)
                s_pool.close()
                return res.json({result:false})
               
            } else {
                console.log("\r\n"+ req.body.db_name+  " ==> Connect OK")
                s_pool.close()
                return res.json({result:true})
            }
        });


    } catch (err) {
        // ... error checks
    }   

});



red_app.post('/query/:db', (req, res) => {
    console.log(req.params.db)

    console.log(req.body)
    try {
        // var query_item = {
        //     query:req.body.sql
        // }
        var query_cmd = req.body.sql


        var index = config_arr.findIndex(function (item, i) {
            return item.con_name === req.params.db
        });
        console.log(index);
        messageHandler(index, query_cmd).then((result_data) => {
            console.log("DB Con: " + result_data.con_index)
            console.log(result_data)
            return res.json(result_data.payload);
        })

    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }

});


red_app.post('/user', (req, res) => {
    try {
        user_arr = req.body
        fs.writeFile("./user.json", JSON.stringify(user_arr), function (err) {
            if (err) {
                console.log(err)
            };
            return res.json({ ret: "save ok" });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});

red_app.get('/user', (req, res) => {
    res.json(user_arr);
});

//===========================================
//         Generate Query Mapping      
//===========================================


async function mapCode3(r_data, table, pwa_info, type, station_code, station_name) {
    var arr = []

    var dev = r_data['Device'];
    if (dev === undefined) {
        return []
    }
    var name = "1"

    // Check Num
    if (!isNaN(parseInt(dev[dev.length - 1])) && check_number(dev[dev.length - 1])) {
        name = parseInt(dev[dev.length - 1])
    }

    try {
        if (r_data['ID'] !== undefined) { delete r_data['ID']; }
        //if( r_data['DataDateTime']  !== undefined)  {delete r_data['DataDateTime'];}
        if (r_data['AggregateType'] !== undefined) { delete r_data['AggregateType']; }
        if (r_data['ShiftID'] !== undefined) { delete r_data['ShiftID']; }
        if (r_data['CalcSta'] !== undefined) { delete r_data['CalcSta']; }
        if (r_data['SendReady'] !== undefined) { delete r_data['SendReady']; }
        if (r_data['Device'] !== undefined) { delete r_data['Device']; }

        //var dev_arr = dev.split("_");
        var reg = pwa_info.reg
        var ww = pwa_info.ww




        for (const item in r_data) {
            var bb = {
                branch_code: ww.toString(),
                station_code: station_code,
                station_type: type,
                station_name: station_name,
                device_code: "", //Ex FLOW_TOTAL-2|   FLOW
                name: name,       //               |   2
                code: "",        //               |   TOTAL
                tag: "",
                map: "",
                c: 1,
                dup: true,
                collect: true,
                topic: ""
            }
            if ((r_data[item] !== null)) {
                bb.map = item

                if (item == "DataDateTime") {
                    bb.collect = false
                }


                // Process Field
                var p_item = item.split('-').join('_')
                p_item = p_item.split('_')

                console.log("\r\n\r\n========  Before Del  =========")
                console.log(p_item)
                console.log(p_item[p_item.length - 1])
                // Check Num
                if (!isNaN(parseInt(p_item[p_item.length - 1]))) {
                    bb.name = parseInt(p_item[p_item.length - 1])
                    //delete p_item[p_item.length-1]
                    p_item.pop()
                }
                console.log("\r\n========  After Del  =========")
                console.log(p_item)
                //bb.device_code = p_item[0]

                // MapCode Field
                if (p_item.length == 1) {
                    var ss = await uds.find(template3, function (temp) { return temp.GROUP == p_item[0]; });
                    if (ss !== undefined) {
                        bb.device_code = ss.device_code
                        bb.code = ss.code
                    }
                }

                if (p_item.length == 2) {
                    // Check Num
                    var ss1 = await uds.find(template3, function (temp) { return temp.GROUP == p_item[0]; });
                    if (ss1 !== undefined) {
                        var ss2 = uds.find(ss1.MAP, function (temp) { return temp.H1 == p_item[1]; });
                        if (ss2 !== undefined) {
                            bb.device_code = ss2.device_code
                            bb.code = ss2.code
                        }
                    }
                }

                if (p_item.length == 3) {
                    var ss1 = await uds.find(template3, function (temp) { return temp.GROUP == p_item[0]; });
                    if (ss1 !== undefined) {
                        var ss2 = uds.find(ss1.MAP, function (temp) { return temp.H1 == p_item[1]; });
                        console.log(ss2);
                        if (ss2 !== undefined) {
                            var ss3 = uds.find(ss2.MAP, function (temp) { return temp.H2 == p_item[2]; });
                            console.log(ss3)
                            bb.device_code = ss3.device_code
                            bb.code = ss3.code
                        }
                    }
                }

                bb.topic = "SCADA/" + ww + "/" + station_code + "/" + type + "/" + station_name + "/" + bb.device_code + "/" + bb.name + "/" + bb.code
                //+ template3[table+'_TOPIC'][item.replace("-","_")]

                arr.push(bb)
            }
        }


    } catch (err) {
        console.log(err)
    }
    return arr;
}

async function mapCodePower(r_data, table, pwa_info, type, station_code, station_name) {
    var arr = []

    var dev = r_data['Device'];
    var power_index = 1;
    try {
        // Check Num
        dev = dev.split('-').join('_');
        dev = dev.split('_');
        if (!isNaN(parseInt(dev[dev.length - 1]))) {
            power_index = parseInt(dev[dev.length - 1])
        }

        if (r_data['ID'] !== undefined) { delete r_data['ID']; }
        //if( r_data['DataDateTime']  !== undefined)  {delete r_data['DataDateTime'];}
        if (r_data['AggregateType'] !== undefined) { delete r_data['AggregateType']; }
        if (r_data['ShiftID'] !== undefined) { delete r_data['ShiftID']; }
        if (r_data['CalcSta'] !== undefined) { delete r_data['CalcSta']; }
        if (r_data['SendReady'] !== undefined) { delete r_data['SendReady']; }
        if (r_data['Device'] !== undefined) { delete r_data['Device']; }

        //var dev_arr = dev.split("_");
        var reg = pwa_info.reg
        var ww = pwa_info.ww

        // Add Time MAP DataDateTime
        var time_tmp = {
            branch_code: ww.toString(),
            station_code: station_code,
            station_type: type,
            station_name: station_name,
            device_code: "POWER",
            name: power_index,
            code: "TIME",
            tag: "",
            map: "DataDateTime",
            c: 1,
            dup: true,
            collect: false,
            topic: ""
        }
        // time_tmp.topic = "SCADA/" +reg +"/" +ww + "/"+station_code +"/"+ type +"/"+ station_name +"/" + time_tmp.device_code + "/" + time_tmp.name + "/" +time_tmp.code
        time_tmp.topic = "SCADA/" + ww + "/" + station_code + "/" + type + "/" + station_name + "/" + time_tmp.device_code + "/" + time_tmp.name + "/" + time_tmp.code
        if (r_data['DataDateTime'] !== undefined) { delete r_data['DataDateTime']; }
        arr.push(time_tmp)

        for (const item in r_data) {
            let bb = {
                branch_code: ww.toString(),
                station_code: station_code,
                station_type: type,
                station_name: station_name,
                device_code: "POWER", //Ex [VOLT_L1L2] |   POWER
                name: power_index,    //               |   1
                code: "",             //               |   VOLTL1L2
                tag: "",
                map: "",
                c: 1,
                dup: true,
                collect: true,
                topic: ""
            }
            if ((r_data[item] !== null)) {
                bb.map = item

                // Process Field POWER
                var p_item = item.split('-').join('_')
                p_item = p_item.split('_')

                console.log(p_item)

                // MapCode Field
                if (p_item.length == 1) {
                    var ss = await uds.find(template3, function (temp) { return temp.GROUP == p_item[0]; });
                    if (ss !== undefined) {
                        bb.device_code = "POWER"
                        bb.code = ss.code
                    } else {
                        bb.code = p_item[0].toString()
                    }
                }

                if (p_item.length == 2) {
                    // Check Num
                    var ss1 = await uds.find(template3, function (temp) { return temp.GROUP == p_item[1]; });
                    if (ss1 !== undefined) {
                        bb.device_code = ss1.device_code
                        bb.code = p_item[0] + ss1.code
                    } else {
                        var tmp = p_item[0] + p_item[1]
                        bb.code = tmp.toString()
                    }
                }


                // p_item = p_item.split('_').join('')

                console.log("\r\n\r\n========  Power Param  =========")
                console.log(p_item)

                // bb.code = p_item
                // bb.topic = "SCADA/" +reg +"/" +ww + "/"+station_code +"/"+ type +"/"+ station_name +"/" + bb.device_code + "/" + bb.name + "/" +bb.code
                bb.topic = "SCADA/" + ww + "/" + station_code + "/" + type + "/" + station_name + "/" + bb.device_code + "/" + bb.name + "/" + bb.code
                arr.push(bb)
            }
        }


    } catch (err) {
        console.log(err)
    }
    return arr;
}

async function mapCodePump3(r_data, pump_all, pwa_info, type, station_code, station_name) {
    var arr = []

    var dev = r_data['Device'];

    try {
        if (r_data['ID'] !== undefined) { delete r_data['ID']; }
        //if( r_data['DataDateTime']  !== undefined)  {delete r_data['DataDateTime'];}
        if (r_data['AggregateType'] !== undefined) { delete r_data['AggregateType']; }
        if (r_data['ShiftID'] !== undefined) { delete r_data['ShiftID']; }
        if (r_data['CalcSta'] !== undefined) { delete r_data['CalcSta']; }
        if (r_data['SendReady'] !== undefined) { delete r_data['SendReady']; }
        if (r_data['Device'] !== undefined) { delete r_data['Device']; }

        //var dev_arr = dev.split("_");
        var reg = pwa_info.reg
        var ww = pwa_info.ww
        var pump = []

        // Add Time MAP DataDateTime
        var time_tmp = {
            branch_code: ww.toString(),
            station_code: station_code,
            station_type: type,
            station_name: station_name,
            device_code: "PUMP",
            name: "1",
            code: "TIME",
            tag: "",
            map: "DataDateTime",
            c: 1,
            dup: true,
            collect: false,
            topic: ""
        }
        // time_tmp.topic = "SCADA/" +reg +"/" +ww + "/"+station_code +"/"+ type +"/"+ station_name +"/" + time_tmp.device_code + "/" + time_tmp.name + "/" +time_tmp.code
        time_tmp.topic = "SCADA/" + ww + "/" + station_code + "/" + type + "/" + station_name + "/" + time_tmp.device_code + "/" + time_tmp.name + "/" + time_tmp.code
        arr.push(time_tmp)


        for (var i = 0; i < pump_all.length; i++) {
            // if(pump_all[i].st === type){
            pump.push(pump_all[i]);
            // }
        }

        for (var j = 1; j <= pump.length; j++) {

            //Check COMMU , RUN_HR , STATUS 
            var m = 'M0' + j
            var m_runhr = 'M0' + j + "_RUN_HR"
            var m_commu = "COMMU0" + j

            if (j >= 10) {
                m = 'M' + j
                m_runhr = 'M0' + j + "_RUN_HR"
                m_commu = "COMMU" + j
            }

            //COMMU
            // if (r_data[m_commu] !== null) {
            //     var bb = {
            //         branch_code: ww.toString(),
            //         station_code: station_code,
            //         station_type: type,
            //         station_name: station_name,
            //         device_code: 'PUMP', //Ex M01_RUN_HR  |   PUMP
            //         name: pump[j - 1].st_code,
            //         code: 'COMMU',
            //         tag: '',
            //         map: m_commu,
            //         c: 1,
            //         dup: true,
            //         collect: true,
            //         topic: "SCADA/" + ww + "/" + station_code + "/" + type + "/" + station_name + "/PUMP/" + pump[j - 1].st_code + "/" + 'COMMU'
            //     }
            //     arr.push(bb);
            // }

            //STATUS
            if (r_data[m] !== null) {
                var bb = {
                    branch_code: ww.toString(),
                    station_code: station_code,
                    station_type: type,
                    station_name: station_name,
                    device_code: 'PUMP', //Ex M01_RUN_HR  |   PUMP
                    name: pump[j - 1].st_code,
                    code: 'STATUS',
                    tag: '',
                    map: m,
                    c: 1,
                    dup: true,
                    collect: true,
                    topic: "SCADA/" + ww + "/" + station_code + "/" + type + "/" + station_name + "/PUMP/" + pump[j - 1].st_code + "/" + 'STATUS'
                }
                arr.push(bb);
            }

            //RUN_HR
            if (r_data[m_runhr] !== null) {
                var bb = {
                    branch_code: ww.toString(),
                    station_code: station_code,
                    station_type: type,
                    station_name: station_name,
                    device_code: 'PUMP', //Ex M01_RUN_HR  |   PUMP
                    name: pump[j - 1].st_code,
                    code: 'RUNHR',
                    tag: '',
                    map: m_runhr,
                    c: 1,
                    dup: true,
                    collect: true,
                    topic: "SCADA/" + ww + "/" + station_code + "/" + type + "/" + station_name + "/PUMP/" + pump[j - 1].st_code + "/" + 'RUNHR'
                }
                arr.push(bb);
            }
        }
    } catch (err) {
        console.log(err)
    }
    return arr;
}

function check_number(input) {

    strLen = input.length
    while (--strLen) {
        switch (input.charAt(strLen)) {
            case "0": case "1": case "2": case "3": case "4": case "5": case "6": case "7": case "8": case "9": return true;
        }
    }
    return false;

}

async function mapMaping2(req_query, data) {
    console.log("\r\n\r\n||==== Map Maping ====||");
    console.table(data.length);
    console.table(req_query);


    var gg = uds.groupBy(data, function (num) { return num.Device; });
    var keys = Object.keys(gg);
    console.log(keys)
    console.log("||===================||");
    console.log("\r\n\r\n")

    var ret_arr = []
    for (const item in gg) {
        var raw = gg[item][0]

        // Device Field Pattern
        // 5511011_BP1_DPB
        // 5511011_BP1_DPB_POWER-1
        // 5511011_BP1_DPB_POWER-2
        // 5511011_BP1_RWP
        // 5511011_PT_DPB_WQ
        // 5511011_PT_DPB_WQ-1
        // 5511011_BP1_WQ
        // 5511011_BP1_WQ-1
        // 5511011_BP1_DPB
        if (raw['Device'] !== undefined) {
            var device_field = raw['Device'].split('-').join('_')
            var qr_name = device_field.split('_');
            console.log("\r\n\r\n" + qr_name[0]) // WW_CODE
            console.log(qr_name[1]) // st_code

            //Search Key !!!!
            var st_cc = "DPB" //init
            var pump = []
            var found_index = await uds.findWhere(info_arr, { 'ww': qr_name[0] });
            console.log(found_index)

            if (found_index !== undefined) {
                var found_station = await uds.findWhere(found_index.station, { 'st_code': qr_name[1] });
                console.log(found_station)

                if (found_station !== undefined) {

                    if (found_station.pump !== undefined) {
                        pump = found_station.pump
                    }

                    var type_check = found_station.st_count
                    for (const type in type_check) {
                        if (type_check[type] < 1) {
                            delete type_check[type];
                        }
                    }
                    console.log(type_check); // Not Use

                    if (type_check.HSP !== undefined) {
                        st_cc = 'HSP'
                    }

                    if (type_check.BOOSTER !== undefined) {
                        st_cc = 'BOOSTER'
                    }

                    if (type_check.FILTATION !== undefined) {
                        st_cc = 'FILTATION'
                    }

                    if (type_check.RWP !== undefined) {
                        st_cc = 'RWP'
                    }

                    if (type_check.DPB !== undefined) {
                        st_cc = 'DPB'
                    }



                    var tmp =
                    {
                        name: "",
                        type: st_cc,
                        code: qr_name[1],
                        station_name: 1,
                        sql_map: [],
                        data: []
                    }



                    if (check_number(qr_name[2])) {
                        tmp.station_name = qr_name[2].charAt(qr_name[2].length - 1)
                    }

                    if (qr_name[3] !== undefined) {

                        if (check_number(qr_name[3])) {
                            tmp.station_name = qr_name[3]
                        }

                    }

                    delete qr_name[0]
                    var last_chr = 0

                    var sql_map = qr_name.join('_')
                    sql_map = sql_map.substring(1)
                    var name = qr_name.join('_')
                    name = name.substring(1)

                    tmp.sql_map.push(sql_map)
                    tmp.name = name
                    console.log(tmp)

                    switch (req_query.tbl) {
                        case "DATATBL_DPB":
                            tmp.data = await mapCode3(raw, req_query.tbl, found_index, tmp.type, tmp.code, tmp.station_name);
                            break;

                        case "DATATBL_RWP":
                            tmp.type = "RWP"
                            tmp.data = await mapCode3(raw, req_query.tbl, found_index, "RWP", tmp.code, tmp.station_name);
                            break;

                        case "DATATBL_POWER":
                            tmp.data = await mapCodePower(raw, req_query.tbl, found_index, tmp.type, tmp.code, tmp.station_name);
                            break;

                        case "DATATBL_PUMP":
                            tmp.data = await mapCodePump3(raw, pump, found_index, tmp.type, tmp.code, tmp.station_name);
                            break;

                        case "DATATBL_WQ":
                            tmp.data = await mapCode3(raw, req_query.tbl, found_index, tmp.type, tmp.code, tmp.station_name);
                            break;

                        default:
                            tmp.data = await mapCode3(raw, req_query.tbl, found_index, tmp.type, tmp.code, tmp.station_name);
                            break;

                    }
                    ret_arr.push(tmp)
                }
            }
        } else {
            break
        }
    }
    return ret_arr;
}

async function mapQuery2(req_query, data) {
    console.log("||==== Map Query ====||");
    console.table(req_query);


    var ret_arr = []
    var gg = await uds.groupBy(data, function (num) {
        return num.Device;
    });

    //console.log(Object.keys(gg).length);
    var keys = Object.keys(gg);
    console.log(keys);
    console.log("||===================||\r\n");

    for (var x = 0; x < keys.length; x++) {
        var qr_name = keys[x].split('_');

        var tmp = {
            //            "query_name": qr_name[1]+'_'+qr_name[2]+'_',//req_query.code + '_'+x,
            // "query_name": keys[x],
            "query_name": "",
            "table": "dbo." + req_query.tbl,
            "type": "MSSQL",
            "dbcon": req_query.con_name,
            "logtable": req_query.tbl,
            "query": "",
            "tabs_name": ""
            // "tabs_name": keys[x]//req_query.code + '_'
        }

        if (qr_name.length <= 3) {
            tmp.query_name = qr_name[1] + "_" + qr_name[2]
            tmp.tabs_name = qr_name[1] + "_" + qr_name[2]

        } else {
            tmp.query_name = qr_name[1] + "_" + qr_name[2] + "_" + qr_name[3]
            tmp.tabs_name = qr_name[1] + "_" + qr_name[2] + "_" + qr_name[3]
        }
        tmp.tabs_name = tmp.tabs_name.replace('-', '_');
        tmp.query_name = tmp.query_name.replace('-', '_');

        switch (req_query.tbl) {
            case "DATATBL_DPB":
                // tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_DPB WHERE Device LIKE '%_" + qr_name[1] + "_%' ORDER BY DataDateTime DESC;"
                tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_DPB WHERE Device LIKE '%_" + tmp.query_name + "' ORDER BY DataDateTime DESC;"

                break;

            case "DATATBL_RWP":
                //tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_RWP WHERE Device LIKE '%_" + qr_name[1] + "_%' ORDER BY DataDateTime DESC;"
                tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_RWP WHERE Device LIKE '%_" + tmp.query_name + "' ORDER BY DataDateTime DESC;"
                break;
            case "DATATBL_POWER":
                tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_POWER WHERE Device LIKE '%_" + qr_name[1] + "_" + qr_name[2] + "_" + qr_name[3] + "%' ORDER BY DataDateTime DESC;"
                break;
            case "DATATBL_PUMP":
                tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_PUMP WHERE Device LIKE '%_" + qr_name[1] + "_" + qr_name[2] + "_" + qr_name[3] + "%' ORDER BY DataDateTime DESC;"
                break;
            case "DATATBL_WQ":
                if (qr_name.length <= 3) {
                    tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_WQ WHERE Device LIKE '%_" + qr_name[1] + "_%' ORDER BY DataDateTime DESC;"
                } else {
                    tmp.query = "SELECT TOP(1) * FROM dbo.DATATBL_WQ WHERE Device LIKE '%_" + qr_name[1] + "_" + qr_name[2] + "_" + qr_name[3] + "%' ORDER BY DataDateTime DESC;"
                }
            default:
                tmp.query = "SELECT TOP(1) * FROM dbo." + req_query.tbl + " WHERE Device LIKE '%" + keys[x] + "%' ORDER BY DataDateTime DESC;"
                break;
        }

        ret_arr.push(tmp)
    }
    return ret_arr;
}


async function loop_gen(req_query) {
    console.log("\r\n===========  Loop Gen  ==============")

    var loop_arr = [];
    var loop_arr_map = [];

    for (let j = 0; j < req_query.length; j++) {

        if (req_query.length > 0) {
            // Add Query 
            // console.table( req_query[j])


            let result = await messageHandler(req_query[j].index, req_query[j].sql);
            console.log("\r\n" + result.payload.length)

            if (result.payload.length > 0) {
                //console.log(result.payload[0].Device)
                var bbb = await mapQuery2(req_query[j], result.payload)
                var map = await mapMaping2(req_query[j], result.payload)
                loop_arr = loop_arr.concat(bbb);
                loop_arr_map = loop_arr_map.concat(map)
            }
        }
    }
    query_config = loop_arr;
    map_config = loop_arr_map;

    await fs.writeFileSync(configDir + "/query.json", JSON.stringify(loop_arr));
    await fs.writeFileSync(configDir + "/mapping.json", JSON.stringify(loop_arr_map));

    console.log("\r\n\r\n=======================================\r\n!!!!! End Query Map !!!!!")
    return map_config;
}




//===========================================
//       TCP  Connection to Node-RED
//===========================================

// TCP Connection
const client = new net.Socket();
client.bytesWritten = 4095
var interval_connect = false;

function launch_Interval_connect() {
    if (false != interval_connect) return
    interval_connect = setInterval(tcp_connect, 5000)
}

function clear_Interval_connect() {
    if (false == interval_connect) return
    clearInterval(interval_connect)
    interval_connect = false
}

function tcp_connect() {
    client.connect({
        port: 4400,
        host: '127.0.0.1'
    })
}

client.on('connect', function () {
    clear_Interval_connect();
    console.log('TCP Connected');
    //interval_connect = true

});

client.on('close', function () {
    console.log('TCP Connection closed');
    launch_Interval_connect()
});

client.on('error', function () {
    console.log('TCP Connection Error');
});

//===========================================
//       Connection Pool  MongoDB      
//===========================================
async function init_mongodb_connection() {
    //Check db_name
    if (dbcon.con[0].db_name !== undefined) {
        mongo_db = dbcon.con[0].db_name
    }

    //Check ip
    if (dbcon.con[0].ip !== undefined) {
        mongo_ip = dbcon.con[0].ip
    }

    //Check port
    if (dbcon.con[0].port !== undefined) {
        mongo_port = dbcon.con[0].port
    }

    //Check user
    if (dbcon.con[0].user !== undefined) {
        mongo_user = dbcon.con[0].user
    }

    //Check pass
    if (dbcon.con[0].pass !== undefined) {
        mongo_pass = dbcon.con[0].pass
    }
    var mongo_url = "mongodb://" + mongo_ip + ":" + mongo_port + "/" + mongo_db + "?authSource=pwa"
    var mongo_option = {
        auth: {
            user: mongo_user,
            password: mongo_pass
        },
        useNewUrlParser: true
    }

    console.log("\r\n #####  " + mongo_url + "\r\n")

    mongoose.connect(mongo_url, mongo_option).then(
        (data) => { /** ready to use. The `mongoose.connect()` promise resolves to mongoose instance. */
            console.log("\r\n ############   Mongo Connect OK!!!  ############  \r\n")
            if (data !== undefined) {
                dbcon.con[0].status = true
            }
        },
        err => {
            /** handle initial connection error */
            console.error("\r\n ############   Mongo Connect Error!!!  ############  \r\n")
            console.log(err)
            if (err !== undefined) {
                dbcon.con[0].status = false
            } else {
                dbcon.con[0].status = true
            }
        }
    );

}

async function remove_null(data){
    console.log(data)
    var tmp = data[0]
    // await data[0].each(obj,function(key,value){
    //     if(value==="" || value===null)
    //     delete obj[key]
    // })
    for(k in tmp){
        if(tmp[k]=== undefined || tmp[k]===null){
            delete tmp[k]
        }
    }
    console.log(tmp)
    var ret = [tmp]
    return ret
}

//===========================================
//       Connection Pool  MS SQL Server      
//===========================================

async function init_db_connection() {
    config_arr = []
    pool_arr = []
    pool_connect = []
    console.log("\r\n Init DB CON!!!")
    console.log(dbcon.con)

    dbcon.con.forEach((con_info, i) => {
        if (con_info.type == "MICROSOFT_DB") {
            config_arr.push({
                index: i - 1,
                con_name: con_info.connection_name,
                user: con_info.user,
                password: con_info.pass,
                server: con_info.ip,
                database: con_info.db_name,
                connectionTimeout: 800,
                pool: {
                    max: 50,
                    min: 0,
                    idleTimeoutMillis: 800
                }
            });
        }
    });

    for (let i = 0; i < config_arr.length; i++) {
        var pool = new sql.ConnectionPool(config_arr[i]);
        pool_arr.push(pool)

        var tmp_connect = pool_arr[i].connect(err => {
            if (err != null) {
                console.log("\r\nDB Connect Error")
                console.log(err.code)
                dbcon.con[i + 1].status = false
            } else {
                console.log("\r\n" + i + ":" + config_arr[i].database + " ==> Connect OK")
                dbcon.con[i + 1].status = true
            }
        });

        //pool_connect.push(tmp_connect);
    }

    console.log("\r\n\r\n//// Config DB ////\r\n")
    console.log(config_arr);
}

async function testPool(index, sqlquery_arr) {
    console.log("\r\n\r\n======================")
    console.log("testPool Length :")
    console.log(sqlquery_arr.length)
    const pool = new sql.ConnectionPool(config_arr[index]);
    pool.on('error', err => {
        // ... error handler 
        console.log('sql errors', err);
    });

    try {
        await pool.connect();

        for (let i = 0; i < sqlquery_arr.length; i++) {
            console.log("\r\n" + i + " :======================\r\n" + sqlquery_arr[i].query_name)
            let result = {
                payload: {},
                map_db: sqlquery_arr[i],
                con_index: index,
                timestamp: moment().valueOf()
            }
            try {
                let result_data = await pool.request().query(sqlquery_arr[i].query);
                // console.log(result_data)
                if (result_data.recordset !== undefined) {
                    result.payload = result_data.recordset
                    //console.log(result_data.recordset)
                    //result.payload = await remove_null(result_data.recordset)
                    //console.log(result_data.payload)

                    client.write(Buffer.from(JSON.stringify(result)),function(err){
                        if(err){console.log(err)}
                    });
                }
            } catch (error) {
                console.log(error)
            }
        }

        //   return result;
    } catch (err) {
        console.log(err)
        return { err: err };
    } finally {
        console.log("\r\n=========================")
        console.log("Connection Close !!!")
        console.log("=========================\r\n")
        pool.close(); //closing connection after request is finished.
    }
    return "";
};



async function messageHandlerSend(index, qry_cmd) {
    // /await pool_connect[index]; // ensures that the pool has been created
    await pool_arr[index];
    let result = {
        payload: {},
        map_db: qry_cmd,
        con_index: index,
        timestamp: moment().valueOf()
    }
    try {
        //const request = pool1.request(); // or: new sql.Request(pool1)
        var request = new sql.Request(pool_arr[index])
        request.canceled = true
        //var request = new sql.Request(pool_connect[index])
        console.log(qry_cmd.query)
        var result_data = await request.query(qry_cmd.query)
        result.payload = result_data.recordset
        return result;

        /*
        request.query(qry_cmd.query).then(function (result_data){
            result.payload = result_data.recordset 
            return result_data;
            
        }).catch(function(err) {
            console.error(err);
            return result;
        });
        */

        //var result_data = await pool_connect[index].request().query(qry_cmd.query)
        //console.log(result_data)

    } catch (err) {
        console.error(err);
        //console.error('SQL error :',qry_cmd.query_name );
        dbcon.con[index + 1].status = false
        return result;
    }
}


async function messageHandler(index, qry_cmd) {
    await pool_connect[index]; // ensures that the pool has been created
    try {
        //const request = pool1.request(); // or: new sql.Request(pool1)
        var request = new sql.Request(pool_arr[index])
        //var result_data = await pool_connect[index].request().query(qry_cmd)
        var result_data = await request.query(qry_cmd)

        var result = {
            payload: result_data.recordset,
            map_db: qry_cmd,
            con_index: index,
            timestamp: moment().valueOf()
        }
        dbcon.con[index].status = true
        return result;
    } catch (err) {
        console.error('SQL error', err);

    }
}

async function query_data() {
    console.log("\r\n Start Query Data Length: ");
    console.log(query_config.length)
    console.log(new Date().toISOString())
    let count = 0;
    //send_data_task()


    for (const query_item of query_config) {
        let index = config_arr.findIndex(function (item, i) {
            return item.con_name === query_item.dbcon
        });

        count = count + 1

        console.log("\r\n" + query_item.query_name + " : " + count)

        let result_data = await messageHandlerSend(index, query_item)

        try {
            // console.log(result_data.payload[0].Device)
            // console.log(result_data.payload[0].DataDateTime)
            console.log(result_data)

            if (result_data.payload.length > 0) {
                client.write(Buffer.from(JSON.stringify(result_data)));
                //send_socket_arr.push(result_data)
            }
        } catch (error) {
            console.log(error)
            //console.log("error :"+result_data.map_db)
        }
    }

    count = 0

    console.log("\r\n End Query !!!")
    console.log(new Date().toISOString())

}

async function query_data_v2() {
    console.log("\r\n Start Query Data Length: ");
    console.log(query_config.length)
    console.log(new Date().toISOString())

    var all_db = uds.groupBy(query_config, function (num) { return num.dbcon; });
    var keys = Object.keys(all_db);
    //console.log(all_db)
    console.log(keys)
    let q = 0;
    for (item in all_db) {
        await testPool(q, all_db[item])
        q = q + 1
    }

    //console.log(ret)

}

async function send_data_task() {
    send_socket_arr.forEach(result_data => {
        client.write(Buffer.from(JSON.stringify(result_data)));
        client.write('*');
    });
    send_socket_arr = []

}

async function sql_loop_query() {
    // setInterval(query_data, 60000);
    setInterval(query_data_v2, 60000);
}


//Connect TCP Server 4400
tcp_connect()
sql_loop_query()


// Create a server
var server = http.createServer(red_app);
server.listen(listenPort, '0.0.0.0');
